package ru.volova.notes.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.volova.notes.parameter.ItemUpdateParameter;
import ru.volova.notes.service.ItemService;
import ru.volova.notes.utils.Result;
import ru.volova.notes.utils.result.ItemResult;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/notes")
public class ItemController {
    @Autowired
    private ItemService itemService;

    @PostMapping("/item")
    public ItemResult create(@RequestBody ItemUpdateParameter itemParameter) {
        return itemService.create(itemParameter);
    }

    @PutMapping("/item")
    public ItemResult update(@RequestBody ItemUpdateParameter itemParameter) {
        return itemService.update(itemParameter);
    }

    @DeleteMapping("/item/{id}")
    public Result delete(@PathVariable Long id) {
        return itemService.delete(id);
    }
}
