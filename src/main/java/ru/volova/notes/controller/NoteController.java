package ru.volova.notes.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.volova.notes.model.Note;
import ru.volova.notes.parameter.NoteParameter;
import ru.volova.notes.parameter.NoteUpdateParameter;
import ru.volova.notes.service.NoteService;
import ru.volova.notes.utils.result.NoteResult;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class NoteController {
    @Autowired
    private NoteService noteService;

    @GetMapping("/notes")
    public NoteResult getAllNotDeleted() {
        return noteService.getAllNotDeleted();
    }

    @GetMapping("/notes/{id}")
    public NoteResult getById(@PathVariable Long id) {
        return noteService.getById(id);
    }

    @PutMapping("/notes")
    public NoteResult update(@RequestBody NoteUpdateParameter noteUpdateParameter) {
        return noteService.update(noteUpdateParameter);
    }

    @DeleteMapping("/notes/{id}")
    public NoteResult delete(@PathVariable Long id) {
        return noteService.delete(id);
    }

    @PostMapping("/notes/add")
    public NoteResult create(@RequestBody NoteParameter noteParameter) {
        return noteService.create(noteParameter);
    }

    @GetMapping("/notes/deleted")
    public NoteResult getAllDeletedNotes() {
        return noteService.getAllDeleted();
    }
}
