package ru.volova.notes.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.volova.notes.parameter.RecipeParameter;
import ru.volova.notes.parameter.RecipeUpdateParameter;
import ru.volova.notes.service.RecipeService;
import ru.volova.notes.utils.result.RecipeResult;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class RecipeController {

    @Autowired
    private final RecipeService recipeService;

    @GetMapping("/recipes")
    public RecipeResult getAllNotDeleted() {
        return recipeService.getAll();
    }

    @GetMapping("/recipes/{id}")
    public RecipeResult getById(@PathVariable Long id) {
        return recipeService.getById(id);
    }

    @PutMapping("/recipes")
    public RecipeResult update(@RequestBody RecipeUpdateParameter recipeUpdateParameter) {
        return recipeService.update(recipeUpdateParameter);
    }

    @DeleteMapping("/recipes/{id}")
    public RecipeResult delete(@PathVariable Long id) {
        return recipeService.delete(id);
    }

    @PostMapping("/recipes/add")
    public RecipeResult create(@RequestBody RecipeParameter recipeParameter) {
        return recipeService.create(recipeParameter);
    }

    @GetMapping("/recipes/deleted")
    public RecipeResult getAllDeleted() {
        return recipeService.getAllDeleted();
    }
}
