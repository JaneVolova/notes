package ru.volova.notes.exception;

import lombok.Getter;
import ru.volova.notes.utils.ResultStatus;

@Getter
public class AppException extends RuntimeException {
    protected final ResultStatus resultStatus;

    protected final String message;

    public AppException(ResultStatus resultStatus, String message, Object... args) {
        this(null, resultStatus, message.formatted(args));
    }

    public AppException(ResultStatus resultStatus) {
        this(null, resultStatus, resultStatus.getMessage());
    }

//    public AppException(Throwable t) {
//        super(t);
//        resultStatus = ResultStatus.INTERNAL_ERROR;
//        message = ExceptionUtils.getRootCauseMessage(t);
//    }

    public AppException(Throwable t, ResultStatus resultStatus, String message) {
        super(message, t);
        this.resultStatus = resultStatus;
        this.message = message;
    }
}
