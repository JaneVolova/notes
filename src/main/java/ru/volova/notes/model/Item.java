package ru.volova.notes.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @JoinColumn(name = "note_id")
    private Long noteId;
    private Long priority;
    @Column(name = "text")
    private String text;
    @Column(name = "checked")
    private Boolean isChecked;

}
