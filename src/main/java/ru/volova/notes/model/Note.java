package ru.volova.notes.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;
    @Enumerated(EnumType.STRING)
    @Column(name = "note_type")
    private NoteType noteType;
    @OneToMany( mappedBy = "noteId", fetch = FetchType.EAGER)
    private List<Item> items = new ArrayList<>();
    @Column(name = "date_created")
    private LocalDateTime dateCreated;
    @Column(name = "date_edited")
    private LocalDateTime dateEdited;
    @Column(name = "removed")
    private Boolean isRemoved;
    @Column(name = "fixed")
    private Boolean isPinned;
    private String color;

}
