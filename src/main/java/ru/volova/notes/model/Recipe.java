package ru.volova.notes.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;
    @Column(name = "text")
    private String text;
    @Column(name = "date_created")
    private LocalDateTime dateCreated;
    @Column(name = "date_edited")
    private LocalDateTime dateEdited;
    @Column(name = "removed")
    private Boolean isRemoved;
    @Column(name = "fixed")
    private Boolean isPinned;
    private String color;
}
