package ru.volova.notes.parameter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemUpdateParameter {
    private Long itemId;
    private Long noteId;
    private String text;
    private Boolean isChecked;
    private Long priority;
}
