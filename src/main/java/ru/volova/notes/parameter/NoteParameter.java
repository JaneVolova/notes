package ru.volova.notes.parameter;

import lombok.Getter;
import lombok.Setter;
import ru.volova.notes.model.NoteType;

@Getter
@Setter
public class NoteParameter {
    private String name;
    private NoteType noteType;
}
