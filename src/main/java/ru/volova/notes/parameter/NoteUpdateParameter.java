package ru.volova.notes.parameter;

import lombok.Getter;
import lombok.Setter;
import ru.volova.notes.model.NoteType;

@Getter
@Setter
public class NoteUpdateParameter {
    private Long noteId;
    private String name;
    private NoteType noteType;
    private Boolean isPinned;
    private String color;
}
