package ru.volova.notes.parameter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecipeParameter {

    private String name;
    private String text;
}
