package ru.volova.notes.parameter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecipeUpdateParameter {
    private Long recipeId;
    private String name;
    private String text;
    private Boolean isPinned;
    private String color;
}
