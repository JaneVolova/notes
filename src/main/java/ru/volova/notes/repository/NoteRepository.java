package ru.volova.notes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.volova.notes.model.Note;

import java.util.List;
import java.util.Optional;

@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {
    List<Note> findAllByIsRemovedTrue();

    List<Note> findAllByIsRemovedFalse();

    Optional<Note> findNoteByIdAndIsRemovedFalse(Long id);
}
