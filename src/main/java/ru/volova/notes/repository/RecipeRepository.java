package ru.volova.notes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.volova.notes.model.Recipe;

import java.util.List;
import java.util.Optional;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {
    List<Recipe> findAllByIsRemovedFalse();

    List<Recipe> findAllByIsRemovedTrue();

    Optional<Recipe> findRecipeByIdAndIsRemovedFalse(Long id);
}
