package ru.volova.notes.service;

import ru.volova.notes.model.Item;
import ru.volova.notes.parameter.ItemUpdateParameter;
import ru.volova.notes.utils.Result;
import ru.volova.notes.utils.result.ItemResult;

public interface ItemService {
    ItemResult create(ItemUpdateParameter itemParameter);

    ItemResult update(ItemUpdateParameter itemParameter);

    Item getById(Long id);

    Result delete(Long id);

}
