package ru.volova.notes.service;

import ru.volova.notes.parameter.NoteParameter;
import ru.volova.notes.parameter.NoteUpdateParameter;
import ru.volova.notes.utils.result.NoteResult;

public interface NoteService {
    NoteResult getAllNotDeleted();

    NoteResult getById(Long id);

    NoteResult update(NoteUpdateParameter noteUpdateParameter);

    NoteResult create(NoteParameter noteParameter);

    NoteResult delete(Long id);

    NoteResult getAllDeleted();
}