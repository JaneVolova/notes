package ru.volova.notes.service;

import ru.volova.notes.parameter.RecipeParameter;
import ru.volova.notes.parameter.RecipeUpdateParameter;
import ru.volova.notes.utils.result.RecipeResult;

public interface RecipeService {
    RecipeResult getAll();

    RecipeResult getById(Long id);

    RecipeResult update(RecipeUpdateParameter recipeUpdateParameter);

    RecipeResult delete(Long id);

    RecipeResult create(RecipeParameter recipeParameter);

    RecipeResult getAllDeleted();
}
