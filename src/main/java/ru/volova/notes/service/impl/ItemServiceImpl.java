package ru.volova.notes.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.volova.notes.model.Item;
import ru.volova.notes.parameter.ItemUpdateParameter;
import ru.volova.notes.repository.ItemRepository;
import ru.volova.notes.service.ItemService;
import ru.volova.notes.utils.Result;
import ru.volova.notes.utils.ResultStatus;
import ru.volova.notes.utils.result.ItemResult;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static ru.volova.notes.utils.AppUtils.*;

@Service
@RequiredArgsConstructor
public class ItemServiceImpl implements ItemService {
    private final ItemRepository itemRepository;

    @Override
    @Transactional
    public ItemResult create(ItemUpdateParameter itemParameter) {
        checkNull(itemParameter, "parameter");

        List<Item> list = itemRepository.findItemsByNoteId(itemParameter.getNoteId());

        Item item = new Item();
        item.setNoteId(itemParameter.getNoteId());
        item.setText(itemParameter.getText());
        item.setIsChecked(itemParameter.getIsChecked());

        if (list.isEmpty()) {
            item.setPriority(1L);
        } else {
            item.setPriority(list.size() + 1L);
        }
        itemRepository.save(item);

        ItemResult result = new ItemResult();
        result.setItemList(Collections.singletonList(item));
        return fillSuccess(result);
    }

    @Override
    @Transactional
    public ItemResult update(ItemUpdateParameter itemParameter) {
        checkNull(itemParameter, "parameter");

        Item item = getById(itemParameter.getItemId());
        item.setText(itemParameter.getText());
        item.setIsChecked(itemParameter.getIsChecked());
        item.setPriority(itemParameter.getPriority());

        itemRepository.save(item);

        ItemResult result = new ItemResult();
        result.setItemList(Collections.singletonList(item));
        return fillSuccess(result);
    }

    @Override
    @Transactional
    public Item getById(Long id) {
        return itemRepository.findById(id)
                .orElseThrow(() -> getNotFoundEx(Item.class, "id", id));
    }

    @Override
    public Result delete(Long id) {
        itemRepository.deleteById(id);

        Result result = new Result();
        result.setDate(LocalDateTime.now());
        result.setStatus(ResultStatus.SUCCESS);
        result.setHttpStatusCode(200);
        return result;
    }
}

