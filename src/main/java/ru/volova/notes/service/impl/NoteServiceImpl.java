package ru.volova.notes.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.volova.notes.model.Note;
import ru.volova.notes.parameter.NoteParameter;
import ru.volova.notes.parameter.NoteUpdateParameter;
import ru.volova.notes.repository.NoteRepository;
import ru.volova.notes.service.NoteService;
import ru.volova.notes.utils.result.NoteResult;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Collections;

import static ru.volova.notes.utils.AppUtils.*;

@Service
@RequiredArgsConstructor
public class NoteServiceImpl implements NoteService {
    private final NoteRepository noteRepository;

    @Override
    @Transactional
    public NoteResult getAllNotDeleted() {
        NoteResult result = new NoteResult();
        result.setNoteList(noteRepository.findAllByIsRemovedFalse());
        return fillSuccess(result);
    }

    @Override
    @Transactional
    public NoteResult getById(Long id) {
        checkNull(id, "parameter");

        Note note = noteRepository.findNoteByIdAndIsRemovedFalse(id)
                .orElseThrow(() -> getNotFoundEx(Note.class, "id", id));

        NoteResult result = new NoteResult();
        result.setNoteList(Collections.singletonList(note));
        return fillSuccess(result);
    }

    @Override
    @Transactional
    public NoteResult update(NoteUpdateParameter noteUpdateParameter) {
        checkNull(noteUpdateParameter, "parameter");

        NoteResult noteResult = getById(noteUpdateParameter.getNoteId());
        Note note = noteResult.getNoteList().get(0);
        note.setName(noteUpdateParameter.getName());
        note.setNoteType(noteUpdateParameter.getNoteType());
        note.setDateEdited(LocalDateTime.now());
        note.setIsPinned(noteUpdateParameter.getIsPinned());
        note.setColor(noteUpdateParameter.getColor());

        noteRepository.save(note);

        NoteResult result = new NoteResult();
        result.setNoteList(Collections.singletonList(note));
        return fillSuccess(result);
    }

    @Override
    @Transactional
    public NoteResult delete(Long id) {
        checkNull(id, "parameter");

        Note note = getById(id).getNoteList().get(0);
        note.setIsRemoved(true);
        noteRepository.save(note);

        NoteResult result = new NoteResult();
        result.setNoteList(Collections.singletonList(note));
        return fillSuccess(result);
    }

    @Override
    @Transactional
    public NoteResult create(NoteParameter noteParameter) {
        checkNull(noteParameter, "parameter");

        Note note = Note.builder()
                .name(noteParameter.getName())
                .noteType(noteParameter.getNoteType())
                .isRemoved(false)
                .dateCreated(LocalDateTime.now())
                .isPinned(false)
                .color(null)
                .build();

        noteRepository.save(note);

        NoteResult result = new NoteResult();
        result.setNoteList(Collections.singletonList(note));
        return fillSuccess(result);
    }

    @Override
    public NoteResult getAllDeleted() {
        NoteResult result = new NoteResult();
        result.setNoteList(noteRepository.findAllByIsRemovedTrue());
        return fillSuccess(result);
    }
}
