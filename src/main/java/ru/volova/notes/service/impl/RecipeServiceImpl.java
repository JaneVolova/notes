package ru.volova.notes.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.volova.notes.model.Recipe;
import ru.volova.notes.parameter.RecipeParameter;
import ru.volova.notes.parameter.RecipeUpdateParameter;
import ru.volova.notes.repository.RecipeRepository;
import ru.volova.notes.service.RecipeService;
import ru.volova.notes.utils.result.RecipeResult;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Collections;

import static ru.volova.notes.utils.AppUtils.*;

@Service
@RequiredArgsConstructor
public class RecipeServiceImpl implements RecipeService {

    private final RecipeRepository recipeRepository;

    @Override
    @Transactional
    public RecipeResult getAll() {
        RecipeResult result = new RecipeResult();
        result.setRecipeList((recipeRepository.findAllByIsRemovedFalse()));
        return fillSuccess(result);
    }

    @Override
    @Transactional
    public RecipeResult getById(Long id) {
        Recipe recipe = recipeRepository.findRecipeByIdAndIsRemovedFalse(id)
                .orElseThrow(() -> getNotFoundEx(Recipe.class, "id", id));

        RecipeResult result = new RecipeResult();
        result.setRecipeList(Collections.singletonList(recipe));
        return fillSuccess(result);
    }

    @Override
    @Transactional
    public RecipeResult update(RecipeUpdateParameter recipeUpdateParameter) {
        checkNull(recipeUpdateParameter, "parameter");

        RecipeResult recipeResult = getById(recipeUpdateParameter.getRecipeId());
        Recipe recipe = recipeResult.getRecipeList().get(0);
        recipe.setName(recipeUpdateParameter.getName());
        recipe.setText(recipeUpdateParameter.getText());
        recipe.setIsPinned(recipeUpdateParameter.getIsPinned());
        recipe.setColor(recipeUpdateParameter.getColor());
        recipe.setDateEdited(LocalDateTime.now());

        recipeRepository.save(recipe);

        RecipeResult result = new RecipeResult();
        result.setRecipeList(Collections.singletonList(recipe));
        return fillSuccess(result);
    }

    @Override
    @Transactional
    public RecipeResult delete(Long id) {
        checkNull(id, "parameter");

        Recipe recipe = getById(id).getRecipeList().get(0);
        recipe.setIsRemoved(true);
        recipeRepository.save(recipe);

        RecipeResult result = new RecipeResult();
        result.setRecipeList(Collections.singletonList(recipe));
        return fillSuccess(result);
    }

    @Override
    @Transactional
    public RecipeResult create(RecipeParameter recipeParameter) {
        checkNull(recipeParameter, "parameter");

        Recipe recipe = Recipe.builder()
                .name(recipeParameter.getName())
                .text(recipeParameter.getText())
                .dateCreated(LocalDateTime.now())
                .isRemoved(false)
                .isPinned(false)
                .color(null)
                .build();

        recipeRepository.save(recipe);

        RecipeResult result = new RecipeResult();
        result.setRecipeList(Collections.singletonList(recipe));
        return fillSuccess(result);
    }

    @Override
    @Transactional
    public RecipeResult getAllDeleted() {
        RecipeResult result = new RecipeResult();
        result.setRecipeList((recipeRepository.findAllByIsRemovedTrue()));
        return fillSuccess(result);
    }
}
