package ru.volova.notes.utils;


import ru.volova.notes.exception.AppException;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class AppUtils {

    private AppUtils() {
    }

    public static LocalDateTime dateToLocalDateTime(Date date) {
        if (date == null) {
            return null;
        }
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    public static <T extends Result> T fillSuccess(T result) {
        return fillResult(result, ResultStatus.SUCCESS);
    }

    public static <T extends Result> T fillResult(T result, ResultStatus resultStatus) {
        result.setStatus(resultStatus);
        result.setHttpStatusCode(resultStatus.getHttpStatusCode().value());
        return result;
    }

    public static AppException getException(ResultStatus status, String message, Object... args) {
        return new AppException(status, message, args);
    }

    public static void throwNotFoundEx(Class<?> clazz, String field, Object arg) {
        throw getNotFoundEx(clazz, field, arg);
    }

    public static void throwException(ResultStatus status, String message, Object... args) {
        throw getException(status, message, args);
    }

    public static AppException getNotFoundEx(Class<?> clazz, String field, Object arg) {
        return getException(ResultStatus.NOT_FOUND, "Не найден %s с %s = %s", clazz.getSimpleName(), field, arg);
    }

    public static void checkNull(Object param, String paramName) {
        if (param == null) {
            AppUtils.throwException(ResultStatus.EMPTY_PARAMETER, "Не задан %s", paramName);
        }
    }


}
