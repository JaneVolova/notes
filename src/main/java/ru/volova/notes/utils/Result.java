package ru.volova.notes.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Result {
    private LocalDateTime date = LocalDateTime.now();
    private Integer httpStatusCode;
    @JsonIgnore
    private ResultStatus status;

}
