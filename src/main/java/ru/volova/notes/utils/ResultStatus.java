package ru.volova.notes.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ResultStatus {
    SUCCESS("Успешный вызов", HttpStatus.OK),
    //    INVALID_PARAMETER("Неправильно задан параметр", HttpStatus.BAD_REQUEST),
    NOT_FOUND("Не удалось найти", HttpStatus.NOT_FOUND),
    EMPTY_PARAMETER("Не задан параметр", HttpStatus.BAD_REQUEST),
    //    ALREADY_USED("Данные уже есть в базе", HttpStatus.BAD_REQUEST),
    INTERNAL_ERROR("Ошибка во время работы сервиса", HttpStatus.INTERNAL_SERVER_ERROR);

    private final String message;

    private final HttpStatus httpStatusCode;
}
