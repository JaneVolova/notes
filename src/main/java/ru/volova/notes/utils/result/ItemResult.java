package ru.volova.notes.utils.result;

import lombok.Getter;
import lombok.Setter;
import ru.volova.notes.model.Item;
import ru.volova.notes.utils.Result;

import java.util.List;

@Setter
@Getter
public class ItemResult extends Result {
    private List<Item> itemList;
}
