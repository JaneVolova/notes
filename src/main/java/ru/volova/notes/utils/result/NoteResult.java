package ru.volova.notes.utils.result;

import lombok.Getter;
import lombok.Setter;
import ru.volova.notes.model.Note;
import ru.volova.notes.utils.Result;

import java.util.List;

@Setter
@Getter
public class NoteResult extends Result {
    List<Note> noteList;
}
