package ru.volova.notes.utils.result;

import lombok.Getter;
import lombok.Setter;
import ru.volova.notes.model.Recipe;
import ru.volova.notes.utils.Result;

import java.util.List;

@Getter
@Setter
public class RecipeResult extends Result {
    List<Recipe> recipeList;
}
