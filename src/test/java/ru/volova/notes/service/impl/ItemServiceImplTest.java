package ru.volova.notes.service.impl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.volova.notes.model.Item;
import ru.volova.notes.parameter.ItemUpdateParameter;
import ru.volova.notes.repository.ItemRepository;
import ru.volova.notes.utils.result.ItemResult;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class ItemServiceImplTest {

    private static final Long ID = 1L;
    private static final Long PRIORITY = 1L;
    @MockBean
    private ItemRepository itemRepository;
    @Autowired
    private ItemServiceImpl itemService;

    @Test
    void test_create_shouldReturnNewItem() {
        Item expected = Item.builder()
                .noteId(ID)
                .text("text")
                .priority(PRIORITY)
                .isChecked(true)
                .build();
        ItemUpdateParameter parameter = new ItemUpdateParameter();
        parameter.setNoteId(ID);
        parameter.setText("text");
        parameter.setIsChecked(true);
        parameter.setPriority(PRIORITY);

        final ItemResult result = itemService.create(parameter);
        assertEquals(expected.getText(), result.getItemList().get(0).getText());
    }

    @Test
    void test_update_shouldReturnUpdatedItem() {
        List<Item> list = Arrays.asList(Item.builder()
                .noteId(ID)
                .text("text")
                .priority(PRIORITY)
                .isChecked(true)
                .build());
        Item expected = Item.builder()
                .noteId(ID)
                .text("text")
                .priority(PRIORITY + 1)
                .isChecked(true)
                .build();
        when(itemRepository.findItemsByNoteId(ID)).thenReturn(list);
        when(itemRepository.findById(ID)).thenReturn(Optional.ofNullable(list.get(0)));

        ItemUpdateParameter parameter = new ItemUpdateParameter();
        parameter.setNoteId(ID);
        parameter.setItemId(ID);
        parameter.setPriority(PRIORITY + 1);

        final ItemResult result = itemService.update(parameter);
        assertEquals(expected.getPriority(), result.getItemList().get(0).getPriority());
    }
}