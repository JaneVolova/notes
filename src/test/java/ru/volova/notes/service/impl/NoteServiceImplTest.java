package ru.volova.notes.service.impl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.volova.notes.model.Note;
import ru.volova.notes.model.NoteType;
import ru.volova.notes.parameter.NoteParameter;
import ru.volova.notes.parameter.NoteUpdateParameter;
import ru.volova.notes.repository.NoteRepository;
import ru.volova.notes.utils.result.NoteResult;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class NoteServiceImplTest {
    private static final long ID = 1L;
    @MockBean
    private NoteRepository noteRepository;
    @Autowired
    private NoteServiceImpl noteService;

    @Test
    void test_getAllNotDeleted_shouldReturnListNotes() {
        List<Note> list = new ArrayList<>();
        list.add(Note.builder()
                .id(ID)
                .noteType(NoteType.TEXT)
                .name("Note1")
                .isRemoved(false)
                .build());
        when(noteRepository.findAllByIsRemovedFalse()).thenReturn(list);

        final NoteResult result = noteService.getAllNotDeleted();
        assertEquals(list.size(), result.getNoteList().size());
        verify(noteRepository).findAllByIsRemovedFalse();
    }

    @Test
    void test_getByIdAndIsRemovedFalse_shouldReturnNote() {
        Note expected = Note.builder().id(ID)
                .noteType(NoteType.TEXT)
                .name("Note1")
                .isRemoved(false)
                .build();
        when(noteRepository.findNoteByIdAndIsRemovedFalse(ID)).thenReturn(Optional.ofNullable(expected));

        final NoteResult result = noteService.getById(ID);
        assertEquals(expected.getName(), result.getNoteList().get(0).getName());
        verify(noteRepository).findNoteByIdAndIsRemovedFalse(ID);
    }

    @Test
    void test_update_shouldReturnUpdatesNote() {
        Note note = Note.builder().id(ID)
                .noteType(NoteType.TEXT)
                .name("Note1")
                .isRemoved(false)
                .build();

        Note updatedNote = Note.builder().id(ID)
                .noteType(NoteType.TEXT)
                .name("Note2")
                .isRemoved(false)
                .build();
        when(noteRepository.findNoteByIdAndIsRemovedFalse(ID)).thenReturn(Optional.ofNullable(note));

        NoteUpdateParameter parameter = new NoteUpdateParameter();
        parameter.setNoteId(ID);
        parameter.setName("Note2");

        final NoteResult result = noteService.update(parameter);

        assertEquals(updatedNote.getName(), result.getNoteList().get(0).getName());
        verify(noteRepository).findNoteByIdAndIsRemovedFalse(ID);
    }

    @Test
    void test_create_shouldReturnNewNote() {
        Note expected = Note.builder().id(ID)
                .noteType(NoteType.TEXT)
                .name("Note1")
                .isRemoved(false)
                .build();
        NoteParameter parameter = new NoteParameter();
        parameter.setName("Note1");
        parameter.setNoteType(NoteType.TEXT);

        final NoteResult result = noteService.create(parameter);
        assertEquals(expected.getNoteType(), result.getNoteList().get(0).getNoteType());
    }

    @Test
    void test_delete_shouldReturnNote() {
        Note expected = Note.builder().id(ID)
                .noteType(NoteType.TEXT)
                .name("Note1")
                .isRemoved(false)
                .build();

        when(noteRepository.findNoteByIdAndIsRemovedFalse(ID)).thenReturn(Optional.ofNullable(expected));

        final NoteResult result = noteService.delete(ID);
        assertEquals(expected.getIsRemoved(), result.getNoteList().get(0).getIsRemoved());
        verify(noteRepository).save(expected);
    }

    @Test
    void test_getAllDeleted_shouldReturnDeletedListNotes() {
        Note expected = Note.builder().id(ID)
                .noteType(NoteType.TEXT)
                .name("Note1")
                .isRemoved(true)
                .build();

        when(noteRepository.findAllByIsRemovedTrue()).thenReturn(Collections.singletonList(expected));

        final NoteResult result = noteService.getAllDeleted();
        assertEquals(expected.getName(), result.getNoteList().get(0).getName());
    }
}