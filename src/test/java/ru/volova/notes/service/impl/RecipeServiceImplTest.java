package ru.volova.notes.service.impl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.volova.notes.model.Recipe;
import ru.volova.notes.parameter.RecipeParameter;
import ru.volova.notes.parameter.RecipeUpdateParameter;
import ru.volova.notes.repository.RecipeRepository;
import ru.volova.notes.utils.result.RecipeResult;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class RecipeServiceImplTest {
    private static final long ID = 1L;
    @MockBean
    private RecipeRepository recipeRepository;
    @Autowired
    private RecipeServiceImpl recipeService;

    @Test
    void test_getAllNotDeleted_shouldReturnListRecipes() {
        Recipe recipe = Recipe.builder()
                .id(ID)
                .name("recipe 1")
                .text("text recipe 1")
                .isRemoved(false)
                .isPinned(false)
                .dateCreated(LocalDateTime.now())
                .build();

        when(recipeRepository.findAllByIsRemovedFalse()).thenReturn(Collections.singletonList(recipe));

        final RecipeResult result = recipeService.getAll();
        assertEquals(recipe.getName(), result.getRecipeList().get(0).getName());
        verify(recipeRepository).findAllByIsRemovedFalse();
    }

    @Test
    void test_getByIdAndIsRemovedFalse_shouldReturnRecipe() {
        Recipe expected = Recipe.builder()
                .id(ID)
                .name("recipe 1")
                .text("text recipe 1")
                .isRemoved(false)
                .isPinned(false)
                .dateCreated(LocalDateTime.now())
                .build();

        when(recipeRepository.findRecipeByIdAndIsRemovedFalse(ID)).thenReturn(Optional.ofNullable(expected));

        final RecipeResult result = recipeService.getById(ID);
        assertEquals(expected.getName(), result.getRecipeList().get(0).getName());
        verify(recipeRepository).findRecipeByIdAndIsRemovedFalse(ID);
    }

    @Test
    void test_update_shouldReturnUpdatedRecipe() {
        Recipe recipe = Recipe.builder()
                .id(ID)
                .name("recipe 1")
                .text("text recipe 1")
                .isPinned(false)
                .dateCreated(LocalDateTime.now())
                .build();

        Recipe updatedRecipe = Recipe.builder()
                .id(ID)
                .name("updated recipe 1")
                .text("updated text recipe 1")
                .isPinned(true)
                .dateEdited(LocalDateTime.now())
                .build();

        when(recipeRepository.findRecipeByIdAndIsRemovedFalse(ID)).thenReturn(Optional.ofNullable(recipe));

        RecipeUpdateParameter parameter = new RecipeUpdateParameter();
        parameter.setRecipeId(ID);
        parameter.setName("updated recipe 1");
        parameter.setText("updated text recipe 1");
        parameter.setIsPinned(true);

        final RecipeResult result = recipeService.update(parameter);

        assertEquals(updatedRecipe.getName(), result.getRecipeList().get(0).getName());
        verify(recipeRepository).findRecipeByIdAndIsRemovedFalse(ID);
    }

    @Test
    void test_delete_shouldReturnDeletedRecipe() {
        Recipe expected = Recipe.builder()
                .id(ID)
                .name("recipe 1")
                .text("text recipe 1")
                .isPinned(false)
                .dateCreated(LocalDateTime.now())
                .build();

        when(recipeRepository.findRecipeByIdAndIsRemovedFalse(ID)).thenReturn(Optional.ofNullable(expected));

        final RecipeResult result = recipeService.delete(ID);
        assertEquals(expected.getIsRemoved(), result.getRecipeList().get(0).getIsRemoved());
    }

    @Test
    void test_create_shouldReturnRecipe() {
        Recipe expected = Recipe.builder()
                .id(ID)
                .name("recipe 1")
                .text("text recipe 1")
                .isPinned(false)
                .dateCreated(LocalDateTime.now())
                .build();

        RecipeParameter parameter = new RecipeParameter();
        parameter.setName("recipe 1");
        parameter.setText("text recipe 1");

        final RecipeResult result = recipeService.create(parameter);
        assertEquals(expected.getName(), result.getRecipeList().get(0).getName());
    }

    @Test
    void getAllDeleted_shouldReturnDeletedListRecipes() {
        Recipe expected = Recipe.builder()
                .id(ID)
                .name("recipe 1")
                .text("text recipe 1")
                .isPinned(false)
                .dateCreated(LocalDateTime.now())
                .build();

        when(recipeRepository.findAllByIsRemovedTrue()).thenReturn(Collections.singletonList(expected));

        final RecipeResult result = recipeService.getAllDeleted();
        assertEquals(expected.getName(), result.getRecipeList().get(0).getName());
    }
}